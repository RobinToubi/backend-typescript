import { ICharacter, ICharacterDto } from "./character.model";
import { characterService } from "./character.service";
import { AbstractController } from "../common/abstract.controller";

class CharacterController extends AbstractController<
  ICharacter,
  ICharacterDto
> {
  service = characterService;
}

export const characterController = new CharacterController();
