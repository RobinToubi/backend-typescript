import { AbstractMapper } from "../common/abstract.mapper";
import { AbstractRepository } from "../common/abstract.repository";
import { AbstractService } from "../common/abstract.service";
import { characterMapper } from "./character.mapper";
import { ICharacter, ICharacterDto } from "./character.model";
import { characterRepo } from "./character.repo";

class CharacterService extends AbstractService<ICharacter, ICharacterDto> {
  protected get repository(): AbstractRepository<ICharacter> {
    return characterRepo;
  }
  protected get mapper(): AbstractMapper<ICharacter, ICharacterDto> {
    return characterMapper;
  }
}

export const characterService = new CharacterService();
