import Character, { ICharacter } from "./character.model";
import { AbstractRepository } from "../common/abstract.repository";

class CharacterRepository extends AbstractRepository<ICharacter> {
  protected get store(): any {
    return Character;
  }
}

export const characterRepo = new CharacterRepository();
