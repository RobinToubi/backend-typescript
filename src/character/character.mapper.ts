import moment, { Moment } from "moment";
import { ICharacter, ICharacterDto } from "./character.model";
import { AbstractMapper } from "../common/abstract.mapper";

class CharacterMapper extends AbstractMapper<ICharacter, ICharacterDto> {
  dtoToModel(dto: ICharacterDto): ICharacter {
    return {
      id: dto.id,
      firstName: dto.firstName,
      lastName: dto.lastName,
      birthYear: moment().subtract(dto.age, "year").year(),
    };
  }

  modelToDto(model: ICharacter): ICharacterDto {
    return {
      id: model.id,
      firstName: model.firstName,
      lastName: model.lastName,
      age: moment().year() - model.birthYear,
    };
  }
}

export const characterMapper = new CharacterMapper();
