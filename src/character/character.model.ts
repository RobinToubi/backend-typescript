import { sequelize } from "./../config/database";
import { DataTypes, Model } from "sequelize";
import { IDto, IModel } from "../common/abstract.model";

export interface ICharacter extends IModel {
  id: number;
  firstName: string;
  lastName: string;
  birthYear: number;
}

export interface ICharacterDto extends IDto {
  id: number;
  firstName: string;
  lastName: string;
  age: number;
}

class Character extends Model<ICharacter> {
  id: number;
  firstName: string;
  lastName: string;
  birthYear: number;

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

Character.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    birthYear: {
      type: DataTypes.INTEGER,
      defaultValue: null,
    },
  },
  {
    tableName: "characters",
    sequelize,
  }
);

export default Character;
