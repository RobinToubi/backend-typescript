import { ICharacter, ICharacterDto } from './character.model';
import { Router } from 'express';
import { AbstractRouter } from '../common/abstract.router';
import { characterController } from './character.controller';

class CharacterRouter extends AbstractRouter<ICharacter, ICharacterDto> {
    protected get controller() {
        return characterController;
    }
}

export const characterRouter = new CharacterRouter().router;