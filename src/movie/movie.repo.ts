import Movie, { IMovie } from "./movie.model";
import { AbstractRepository } from "../common/abstract.repository";

class MovieRepository extends AbstractRepository<IMovie> {
  protected get store(): any {
    return Movie;
  }
}

export const movieRepo = new MovieRepository();
