import { AnyARecord } from 'dns';
import { NextFunction, Request, Response, response } from "express";
import { AbstractController } from "../common/abstract.controller";
import { ErrorType, IAppError } from "../misc/error.enum";
import { IMovieDto } from "./movie.mapper";
import Movie, { IMovie } from "./movie.model";
import { movieService } from "./movie.service";

class MovieController extends AbstractController<IMovie, IMovieDto> {
  service = movieService;
}

  // get(request: Request, response: Response, next: NextFunction): any {
  //   const id = parseInt(request.params.id);
  //   if (isNaN(id)) {
  //     const error: IAppError = {
  //       type: ErrorType.resourceIdFormat,
  //       messageParam: "IL FAUT UN INT",
  //     };
  //     next(error);
  //   } else {
  //     this.service.get(id).then((movie) => {
  //       response.json(movie);
  //     }).catch((err) => {
  //       const error: IAppError = {
  //         type: ErrorType.resourceIdNotFound,
  //         messageParam: "NON TROUVE",
  //       };
  //       next(error);
  //     });
  //   }
  // }

export const movieController = new MovieController();

//   create(request: Request, response: Response, next: NextFunction): void {
//     serviceCreate(request.body).then((character) => {
//       if (character) {
//         response.json(character);
//       } else {
//         response.status(404);
//         response.json({
//           message: `The ressource wasn't created`,
//         });
//       }
//     });
//   }
// }

// export const movieController = new MovieController();

// export const get = (request, response, next) => {
//   const id = parseInt(request.params.id);
//   if (isNaN(id)) {
//     const error: IAppError = {
//       type: ErrorType.resourceIdFormat,
//       messageParam: "IL FAUT UN INT",
//     };
//     next(error);
//   } else {
//     serviceGet(id).then((character) => {
//       response.json(character);
//       const error: IAppError = {
//         type: ErrorType.resourceIdNotFound,
//         messageParam: "NON TROUVE",
//       };
//       next(error);
//     });
//   }
// };

// export const create = (request, response) => {
//   serviceCreate(request.body).then((character) => {
//     if (character) {
//       response.json(character);
//     } else {
//       response.status(404);
//       response.json({
//         message: `The ressource wasn't created`,
//       });
//     }
//   });
// };

// export const all = (request, response, next) => {
//   serviceFindAll()
//     .then((movies) => {
//       if (movies) response.json(movies);
//     })
//     .catch((err) => {
//       const error: IAppError = {
//         type: ErrorType.resourceIdNotFound,
//         messageParam: "NON TROUVE",
//         sequelizeError: err,
//       };
//       next(error);
//     });
// };
