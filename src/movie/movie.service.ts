import { threadId } from "worker_threads";
import { AbstractMapper } from "../common/abstract.mapper";
import { AbstractRepository } from "../common/abstract.repository";
import { AbstractService } from "../common/abstract.service";
import { IMovieDto, movieMapper } from "./movie.mapper";
import Movie, { IMovie } from "./movie.model";
import { movieRepo } from "./movie.repo";

class MovieService extends AbstractService<IMovie, IMovieDto> {
  protected get repository(): AbstractRepository<IMovie> {
    return movieRepo;
  };
  protected get mapper(): AbstractMapper<IMovie, IMovieDto> {
    return movieMapper;
  };
}

export const movieService = new MovieService();

// export const findAll = (): Promise<Movie[]> => {
//   return movieRepo.findAll().then((movies) => {
//     return movies;
//   });
// };

// export const get = (id): Promise<Movie> => {
//   return movieRepo.get(id).then((movie) => {
//     return movie;
//   });
// };

// export const create = (newMovie: Movie): Promise<Movie> => {
//   return movieRepo.create(newMovie).then((movie) => {
//     return movie;
//   });
// };
