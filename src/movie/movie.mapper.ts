import { AbstractMapper } from "../common/abstract.mapper";
import { IDto } from "../common/abstract.model";
import Movie, { IMovie } from "./movie.model";

export interface IMovieDto extends IDto {
    id: number;
    name: string;
    affiche: string;
    sortie: Date;
}

class MovieMapper extends AbstractMapper<IMovie, IMovieDto> {
  modelToDto(movie: IMovie): IMovieDto {
    return {
      id: movie.id,
      name: movie.name,
      affiche: movie.affiche,
      sortie: movie.dateSortie,
    };
  }

  dtoToModel(movieDto: IMovieDto): IMovie {
    return {
      id: movieDto.id,
      name: movieDto.name,
      dateSortie: movieDto.sortie,
      affiche: movieDto.affiche
    };
  }
}

export const movieMapper = new MovieMapper();
