import { DataTypes, Model, Optional } from "sequelize";
import { IModel } from "../common/abstract.model";
import { sequelize } from "../config/database";

export interface IMovie extends IModel {
  id: number;
  name: string;
  dateSortie: Date;
  affiche: string | null;
}

interface MovieCreationAttributes extends Optional<IMovie, "id"> {}

class Movie extends Model<IMovie> {
  id: number;
  name: string;
  dateSortie: Date;
  affiche: string | null;
  model = this;

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

Movie.init(
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    dateSortie: {
      type: DataTypes.DATEONLY,
      defaultValue: Date.now(),
    },
    affiche: {
      type: DataTypes.STRING,
      defaultValue: null,
    },
  },
  {
    tableName: "movies",
    sequelize,
  }
);

export default Movie;