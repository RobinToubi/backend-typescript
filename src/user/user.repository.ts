import { Model } from 'sequelize';
import { ModelNotFoundError } from "../common/error/repository-error";
import { AbstractRepository } from "../common/abstract.repository";
import User, { IUser } from "../auth/auth.model";
import bcrypt from "bcrypt";

class UserRepository extends AbstractRepository<IUser> {
  protected get store(): any {
    return User;
  }

  create(user: any): Promise<any> {
    return bcrypt.hash(user.password, 10).then((hashedPassword) => {
      user.password = hashedPassword;
      return super.create(user);
    })
  }

  findByEmailAndPassword(email: string, password: string): Promise<IUser> {
    return this.store.findOne({ where: { email: email } }).then((user) => {
      if (!user) {
        throw new ModelNotFoundError();
      }

      return bcrypt.compare(password, user.password).then((result) => {
        if (result) return user;
        else throw new Error();
      });
    });
  }
}

export const userRepository = new UserRepository();
