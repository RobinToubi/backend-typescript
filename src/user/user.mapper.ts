import User, { IAuthToken, IUser } from "../auth/auth.model";
import { ITokenPayload } from "../common/token.service";

export interface IUserDto {
  email: string;
  name: string;
  surname: string;
}

class UserMapper {
  modelToDto(user: User): Promise<IUserDto> {
    if (!user) return Promise.reject("No user");
    return Promise.resolve().then(() => {
      return {
        email: user.email,
        name: user.name,
        surname: user.surname,
      };
    });
  }

  dtoToModel(dto: IUser): Promise<any> {
    return Promise.resolve().then(() => {
      return {
        email: dto.email,
        id: dto.id,
        surname: dto.surname,
        name: dto.name,
        username: dto.username,
        password: dto.password,
      };
    });
  }

  dtoToPayload(dto: IUser): Promise<ITokenPayload> {
    return Promise.resolve().then(() => {
      return {
        email: dto.email,
        id: dto.id
      }
    })
  }
}

export const userMapper = new UserMapper();
