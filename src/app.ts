import express from "express";
import { router } from "./app.router";
import cors from "cors";
import { sequelize } from "./config/database";

export const app = express();

sequelize
  .authenticate()
  .then(() => {
    console.log("Synchronisation en cours...");
    sequelize.sync();
    console.log("Authentication successfull !");
  })
  .catch((err) => {
    console.error("Error on connecting to the database ", err);
  });

app.use(cors());
app.use(express.json());
app.use("/", router);
