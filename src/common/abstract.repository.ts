import { IModel as AbstractModel} from './abstract.model';
import { PrimaryKeyError } from './error/repository-error';

export abstract class AbstractRepository<T extends AbstractModel> {
  protected abstract get store(): any;

  findAll = (): Promise<T[]> => {
    return this.store.findAll().then((data) => {
      return data;
    });
  };

  get(id: number): Promise<T> {
    return this.store.findByPk(id).then((movie) => {
      if (movie)
        return movie;
      throw new PrimaryKeyError();
    });
  };

  create(item: T): Promise<T> {
    return this.store.create(item).then(() => {
      return item;
    });
  };

  update(id: number, item: T): Promise<T> {
    return this.store.update(item, {where: {id: id}}).then((result) => {
      if (result)
        return item;
      throw new PrimaryKeyError();
    })
  }

  remove(id: number): Promise<T> {
    return this.store.destroy({where: {id: id }}).then((result) => {
      if (result) return result;
      throw new PrimaryKeyError();
    })
  }
}
