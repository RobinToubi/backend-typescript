import { ErrorType } from "../../misc/error.enum";
import { PrimaryKeyError } from './repository-error';

export const itemErrorHandler = id => error => {
  if (error instanceof PrimaryKeyError) {
    return Promise.reject({ type: ErrorType.resourceIdNotFound, messageParam: id });
  }
  return Promise.reject(error);
};
