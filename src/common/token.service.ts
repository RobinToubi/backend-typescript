import { sign, verify } from 'jsonwebtoken';

const secretKey = 'abcdef';

export interface ITokenPayload {
  id: string;
  email: string;
}

export const generateToken = (tokenPayload: ITokenPayload): Promise<string> => {
  return new Promise((resolve, reject) => {
    sign(tokenPayload, secretKey, (error, token) => {
      if (error) {
        console.log("oenrgern")
        reject(error);
      } else {
        resolve(token);
      }
    });
  });
}

export const checkToken = (token: string): Promise<ITokenPayload> => {
  return new Promise((resolve, reject) => {
    verify(token, secretKey, (error, payload) => {
      if (error) {
        reject(error);
      } else {
        resolve(payload as ITokenPayload);
      }
    });
  });
}
