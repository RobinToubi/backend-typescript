import { Model, ModelAttributes, ModelType } from "sequelize";
import { AbstractMapper } from "./abstract.mapper";
import { IDto, IModel } from "./abstract.model";
import { AbstractRepository } from "./abstract.repository";
import { itemErrorHandler } from "./error/error.mapper";

export abstract class AbstractService<M extends IModel, D extends IDto> {
  protected abstract get repository(): AbstractRepository<M>;
  protected abstract get mapper(): AbstractMapper<M, D>;

  findAll(): Promise<D[]> {
    return this.repository.findAll().then((models) => {
      return this.mapper.modelsToDtos(models);
    });
  }

  get(id: number): Promise<D> {
    console.log(id);
    return this.repository
      .get(id)
      .then((model) => this.mapper.modelToDto(model))
      .catch(itemErrorHandler(id));
  }

  create(dto: D): Promise<D> {
    const character = this.mapper.dtoToModel(dto);
    return this.repository
      .create(character)
      .then((model) => this.mapper.modelToDto(model));
  }

  update(id: number, dto: D): Promise<D> {
    const character = this.mapper.dtoToModel(dto);
    return this.repository
      .update(id, character)
      .then((model) => this.mapper.modelToDto(model))
      .catch(itemErrorHandler(id));
  }

  remove(id: number): Promise<D> {
    return this.repository
      .remove(id)
      .then((model) => this.mapper.modelToDto(model))
      .catch(itemErrorHandler(id));
  }
}
