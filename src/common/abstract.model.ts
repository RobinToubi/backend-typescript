export interface IModel {
  identifier?: number;
}

export interface IDto {
  id?: number;
}
