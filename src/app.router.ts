import express from "express";
import { characterRouter } from "./character/character.router";
import { errorMiddleware } from "./middlewares/error.middleware";
import { logMiddleware } from "./middlewares/log.middleware";
import { movieRouter } from "./movie/movie.router";
import { authRouter } from "./auth/auth.router";
import { authErrorMiddleware } from "./middlewares/errors/auth.error.middleware";

export const router = express.Router();

router.use(logMiddleware);

router.use("/movies", movieRouter);
router.use("/characters", characterRouter);
router.use("/auth", authRouter);


router.use("/movies", errorMiddleware);
router.use("/characters", errorMiddleware);
router.use("/auth", authErrorMiddleware);


