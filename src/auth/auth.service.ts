import { nextTick } from "node:process";
import { generateToken } from "../common/token.service";
import { ErrorType } from "../misc/error.enum";
import { IUserDto, userMapper } from "../user/user.mapper";
import { userRepository } from "../user/user.repository";
import User, { IAuthToken, IUser } from "./auth.model";

class AuthService {
  register(credentials: IUser): Promise<IUserDto> {
    console.log(credentials);
    return userMapper.dtoToModel(credentials).then((user) => {
      return userRepository.create(user).then((userCreated) => {
        return userMapper.modelToDto(userCreated);
      })
    });
  }

  login(credentials: IUser): Promise<IAuthToken> {
    return userRepository
      .findByEmailAndPassword(credentials.email, credentials.password)
      .then((user) => userMapper.dtoToPayload(user))
      .then((mappedUser) => generateToken(mappedUser))
      .then((token) => ({ token }))
      .catch((error) => {
        return Promise.reject({ type: ErrorType.invalidCredentials });
      });
  }
}

export const authService = new AuthService();
