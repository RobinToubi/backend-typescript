import { ErrorType } from './../misc/error.enum';
import { IAuthToken, IUser } from "./auth.model";
import { NextFunction, Request, Response } from "express";
import { authService } from "./auth.service";
import { v4 } from "uuid";
import { IAppError } from "../misc/error.enum";

class AuthController {
  register(request: Request, response: Response, next: NextFunction): void {
    const credentials: IUser = {
      id: v4(),
      email: request.body.email,
      username: request.body.username,
      password: request.body.password,
      name: request.body.name,
      surname: request.body.surname,
    };
    authService.register(credentials).then((userDto) => {
      response.json(userDto);
    }).catch((err) => {
      const error: IAppError = {
        type: ErrorType.resourceIdFormat,
        messageParam: "Error on creating user",
        sequelizeError: err.message
      };
      next(error);
    });
  }

  login(req: Request, res: Response, next: NextFunction): void {
    const credentials = req.body;
    authService
      .login(credentials)
      .then((dto) => res.json(dto))
      .catch(next);
  }
}

export const authController = new AuthController();
