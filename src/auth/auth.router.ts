import { authController } from "./auth.controller";
import { NextFunction, Request, Response, Router } from "express";

export const authRouter = Router();

authRouter.post(
  "/login",
  (request: Request, response: Response, next: NextFunction) =>
    authController.login(request, response, next)
);

authRouter.post(
  "/register",
  (request: Request, response: Response, next: NextFunction) =>
    authController.register(request, response, next)
);
