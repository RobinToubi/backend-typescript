import { sequelize } from "./../config/database";
import { DataTypes, Model, Optional } from "sequelize";
import { IModel } from "../common/abstract.model";

export interface IUser extends IModel{
  id: string;
  name: string;
  surname: string;
  email: string;
  password: string;
  username: string;
}

export interface IAuthToken {
  token: string;
}

interface UserCreationAttributes extends Optional<User, "id"> {}

class User extends Model<IUser, UserCreationAttributes> implements IUser{
  id: string;
  name: string;
  surname: string;
  email: string;
  password: string;
  username: string;

  readonly createdAt: Date;
  readonly updatedAt: Date;
}

User.init(
  {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    surname: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    username: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    tableName: "users",
    sequelize,
  }
);

export default User;
