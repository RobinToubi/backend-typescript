import { NextFunction, Request, Response } from 'express';
import { IAppError } from '../../misc/error.enum';

export const authErrorMiddleware = (error: IAppError, request: Request, response: Response, next: NextFunction) => {
    response.status(404).json(error);
}