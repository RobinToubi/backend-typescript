export const logMiddleware = (request, response, next) => {
    console.log(`METHOD : ${request.method} | URL : ${request.url} CONTENT TYPE : ${request.headers["content-type"]}`);
    next();
}