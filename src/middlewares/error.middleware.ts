import { Response, Request, NextFunction } from 'express';
import { IAppError } from '../misc/error.enum';


export const errorMiddleware = (error: IAppError, request: Request, response: Response, next: NextFunction) => {
    const err = error.type + " - " + error.messageParam;
    console.log(error);
    response.status(403).json(error);
}
